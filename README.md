# GeoDB

A POC Geo DB with minimal resources
using SQLite and Clojure

## Usage

clone and run with `lein run`

## Example runs

In-memory, 1 million profiles

```
Start
Inserting
"Elapsed time: 83025.637996 msecs"
Creating indexes
"Elapsed time: 2063.550956 msecs"
Number of rows:  1000000
Select rectangle r=20, c=[30.5-30.5]
({:cnt 1600})
"Elapsed time: 45.197355 msecs"
Select circle r=20, c=[30.5-30.5]
({:cnt 1264})
"Elapsed time: 322.420309 msecs"
Select rect+circle r=20, c=[30.5-30.5]
({:cnt 1264})
"Elapsed time: 34.733668 msecs"
N closest profiles rect+circle r=20, c=[30.5-30.5], closest first
({:long 30, :lat 30, :id 30030.0}
{:long 30, :lat 31, :id 30031.0}
{:long 31, :lat 30, :id 31030.0}
{:long 31, :lat 31, :id 31031.0}
{:long 29, :lat 30, :id 29030.0}
{:long 29, :lat 31, :id 29031.0}
{:long 30, :lat 29, :id 30029.0}
{:long 30, :lat 32, :id 30032.0})
"Elapsed time: 36.720749 msecs"
```

On disk, 1 million profiles (file size after: 65 MB)
```
Start
Inserting
"Elapsed time: 182349.952018 msecs"
Creating indexes
"Elapsed time: 8075.123431 msecs"
Number of rows:  1000000
Select rectangle r=20, c=[30.5-30.5]
({:cnt 1600})
"Elapsed time: 46.922428 msecs"
Select circle r=20, c=[30.5-30.5]
({:cnt 1264})
"Elapsed time: 351.098826 msecs"
Select rect+circle r=20, c=[30.5-30.5]
({:cnt 1264})
"Elapsed time: 40.11105 msecs"
N closest profiles rect+circle r=20, c=[30.5-30.5], closest first
({:long 30, :lat 30, :id 3000030.0}
{:long 30, :lat 31, :id 3000031.0}
{:long 31, :lat 30, :id 3100030.0}
{:long 31, :lat 31, :id 3100031.0}
{:long 29, :lat 30, :id 2900030.0}
{:long 29, :lat 31, :id 2900031.0}
{:long 30, :lat 29, :id 3000029.0}
{:long 30, :lat 32, :id 3000032.0})
"Elapsed time: 46.236523 msecs"
```

Yes, insert is slow. The indexes make it 5 times faster in the select (rectangle).

This is just a test

## License

Copyright © 2015 JRamb

