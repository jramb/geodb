(ns geodb.core
  ^{:author "Jörg Ramb"}
  (:require [clojure.java.jdbc :as j])
  )

(defn foo
  "I don't do a whole lot."
  [x]
  (println "Hello," x))

(def db-specs
  {:classname   "org.sqlite.JDBC"
   :subprotocol "sqlite"
   ;:subname     "disk.sqlite" ; on disk
   :subname     ":memory:" ; in-memory 
   })


(defn -main [& args]
  ;(Class/forName "org.sqlite.JDBC") ; NEEDED?
  (println "Start")
  (let [size 1e3]
    (j/with-db-connection [db-con db-specs]
      (j/db-do-commands db-con
                        false ; no need for a transaction
                        "create table if not exists geo_profiles
                        ( id varchar2(32) not null
                        , lat number
                        , long number
                        , primary key (id)
                        );"
                        )
      (time
        (j/with-db-transaction [db db-con]
          (println "Inserting")
          (dorun
            (for [x (range size) y (range size)]
              (j/insert! db :geo_profiles
                         {:id (+ x (* y 1e5)) :lat x :long y})))))
      (time
        (do
          (println "Creating indexes")
          (j/db-do-commands db-con false
                            ; these indexes make ranged queries on lat+long 5 times faster
                            "create index if not exists
                            geo_profiles_n01 on geo_profiles ( lat );"
                            "create index if not exists
                            geo_profiles_n02 on geo_profiles ( long );"
                            )))
      (do
        (println "Number of rows: "
               (j/db-query-with-resultset db-con
                                          ["select count(*) as cnt
                                           from geo_profiles"]
                                          #(:cnt (first (j/result-set-seq %))))))

      (time
        (do
          (println "Select rectangle r=20, c=[30.5-30.5]")
          (println
            (j/db-query-with-resultset
              db-con
              ["select count(*) as cnt
               from geo_profiles
               where lat between ? and ?
               and long between ? and ?"
               10.5 50.5 10.5 50.5]
              #(doall (j/result-set-seq %))
              ))))
      (time
        (do
          (println "Select circle r=20, c=[30.5-30.5]")
          (println
            (j/db-query-with-resultset
              db-con
              ["select count(*) as cnt
               from geo_profiles
               where
               (lat-?)*(lat-?)+(long-?)*(long-?) < ?
               "
               30.5 30.5 30.5 30.5
               (* 20 20)]
              #(doall (j/result-set-seq %))
              )))) 
      (time
        (do
          (println "Select rect+circle r=20, c=[30.5-30.5]")
          (println
            (j/db-query-with-resultset
              db-con
              ["select count(*) as cnt
               from geo_profiles
               where lat between ? and ?
               and long between ? and ? 
               and (lat-?)*(lat-?)+(long-?)*(long-?) < ?
               " 10.5 50.5 10.5 50.5
               30.5 30.5 30.5 30.5
               (* 20 20)
               ]
              #(doall (j/result-set-seq %))
              ))))
     (time
        (do
          (println "N closest profiles rect+circle r=20, c=[30.5-30.5], closest first")
          (println
            (j/db-query-with-resultset
              db-con
              ["select id, lat, long
               from geo_profiles
               where lat between ? and ?
               and long between ? and ? 
               -- the following conditions is pratically not needed here
               and      (lat-?)*(lat-?)+(long-?)*(long-?) < ?
               order by (lat-?)*(lat-?)+(long-?)*(long-?) asc
               limit 8
               "
               ;10.5 50.5 10.5 50.5
               (- 30.5 20.0) (+ 30.5 20.0) (- 30.5 20.0) (+ 30.5 20.0)
               30.5 30.5 30.5 30.5 (* 20.0 20.0)
               30.5 30.5 30.5 30.5
               ]
              #(doall (j/result-set-seq %))
              ))))
     #_(time ;; hmmm this is not faster (but look nicer)
        (do
          (println "N closest profiles rect+circle r=20, c=[30.5-30.5], closest first")
          (println
            (j/db-query-with-resultset
              db-con
              ["select id, lat, long, dlat*dlat+dlong*dlong sdist from
                   (select id, lat, long, abs(lat-?) dlat, abs(long-?) dlong
                     from geo_profiles
                     where lat between ? and ?
                     and long between ? and ?)
               where sdist < ?
               order by sdist asc
               limit 8
               " 30.5 30.5 
               ;10.5 50.5 10.5 50.5
               (- 30.5 20) (+ 30.5 20)
               (- 30.5 20) (+ 30.5 20)
               (* 20 20)
               ]
              #(doall (j/result-set-seq %))
              )))) 
      )))
